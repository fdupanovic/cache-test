.DEFAULT_GOAL := server
.PHONY: build lint shell server test debug

build:
	docker build -t cachedev .

lint: build
	docker run --rm -it -v $$(pwd):/cache cachedev run lint

shell: build
	docker run --rm -it -v $$(pwd):/cache --entrypoint=/bin/bash cachedev

server: lint
	docker run --rm -it -p 8000:8000 -v $$(pwd):/cache cachedev

test: lint
	docker run --rm -it -v $$(pwd):/cache cachedev test

debug: build
	docker run --rm -it -v $$(pwd):/cache cachedev run debug
