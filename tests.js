import TestRunner from './lib/test';
import Resources, {Disposable} from './lib/resource';

var runner = new TestRunner();

runner
.before(function() {
  let TestResource = class {
    constructor(...args) {
      this.createdWithArgs = args;
    }
  };
  let mockCreate = (...args) => new TestResource(...args);

  this.resources = new Resources();
  this.resources.create = mockCreate;

  this.disposable = new Disposable(this.resources, 'foo', 'bar');
})

.add(function initiallyDoesntHaveResource() {
  this.assert(!this.resources.has(0));
})
.add(function getReturnsADisposable() {
  let disposable = this.resources.get(0);

  this.assert(disposable instanceof Disposable);
})
.add(function getCreatesResourceWithId() {
  let disposable = this.resources.get(0);

  this.assert(disposable.resource.createdWithArgs[0] === 0);
})
.add(function getCachesResource() {
  let disposable = this.resources.get(0);

  this.assert(this.resources.get(0).resource === disposable.resource);
})
  .add(function thenHasResource() {
    this.resources.get(0);
    this.assert(this.resources.has(0));
  })
  .add(function canDeleteResource() {
    this.assert(!this.resources.delete(0));

    this.resources.get(0);
    this.assert(this.resources.delete(0));
    this.assert(!this.resources.has(0));
  })
  .add(function newResourceCreatedAferwards() {
    let disposable = this.resources.get(0);
    this.resources.delete(0);

    this.assert(this.resources.create(0) !== disposable.resource);
  })
.add(function Disposable() {
  this.assert(this.disposable.id === 'foo');
  this.assert(this.disposable.resource === 'bar');
})
  .add(function disposed() {
    this.assert(this.disposable.disposed);

    this.resources.get('foo');
    this.assert(!this.disposable.disposed)
  })
  .add(function dispose() {
    this.assert(!this.disposable.dispose());

    this.resources.get('foo');
    this.assert(this.disposable.dispose());
  })
.run();
