'use strict';

import {resource} from '.';
import http from 'http';

process.on('uncaughtException', e => console.error(e.stack));

http
.createServer(function(req, res) {
  console.log(`Got request\n${JSON.stringify(req.headers, null, 2)}`);

  res.writeHead(200, {'Content-Type': 'application/json'});

  let disposable = resource(req.url);
  res.end(JSON.stringify(disposable.resource));
  disposable.dispose();
})
.listen(process.env.PORT, function() {
  console.info('Listening on port:', process.env.PORT);
});
