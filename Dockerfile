FROM node:latest

ENV PATH=/usr/local/lib/node_modules/:/usr/local/lib/node_modules/bin:$PATH \
	PORT=8000

EXPOSE 8000

RUN npm install -g babel eslint babel-eslint

WORKDIR /cache

CMD ["start"]
ENTRYPOINT ["npm"]
