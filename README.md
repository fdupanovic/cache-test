## Requirements

Access to a Docker host.

## Usage

Use `make` or `babel-node` directly on the sources.

See:
* [`npm-package.json`](https://docs.npmjs.com/files/package.json)
* [`npm-scripts`](https://docs.npmjs.com/misc/scripts)
