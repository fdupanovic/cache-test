'use strict';

import Resources from './lib/resource';

const resources = new Resources();
export const resource = resources.get.bind(resources);
