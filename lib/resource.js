const caches = new WeakMap();

// A cache of unclaimed resources.
export default class Resources {
  constructor() {
    caches.set(this, new Map());
  }

  // Returns a resource which can delete itself.
  get(id) {
    let c = cache(this);
    let resource;

    if (!c.has(id)) {
      resource = this.create(id);
      c.set(id, resource);

      return new Disposable(this, id, resource);
    }

    resource = c.get(id);
    return new Disposable(this, id, resource);
  }

  // Membership test.
  has(id) {
    return caches.get(this).has(id);
  }

  // Creates a resource.
  create(id) {
    return {
      value: `I'm an expensive value with ID ${id}`
    }
  }

  // Deletes a resource.
  delete(id) {
    return cache(this).delete(id);
  }
}

function cache(instance) {
  return caches.get(instance);
}

// A resource that can clean up after itself.
export class Disposable {
  constructor(resources, id, resource) {
    this.id = id;
    this.resource = resource;

    Object.defineProperties(this, {
      // Am I still there?
      'disposed': {
        get: () => !resources.has(id)
      },
      // Busted.
      'dispose': {
        value: resources.delete.bind(resources, id)
      }
    });
  }
}
