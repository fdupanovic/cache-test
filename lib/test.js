'use strict';

const state = new WeakMap();
const isNode = typeof process !== 'undefined';

export default class TestRunner {
  constructor() {
    state.set(this, {});

    this.tests = [];
  }

  add(t) {
    this.tests.push(t);

    return this;
  }

  run() {
    this.tests.forEach((t) => {
      let context = {
        assert: this.assert
      };

      before(this).call(context);

      try {
        t.call(context);

        console.info('SUCCESS', t.name);
      } catch(e) {
        console.error(`FAIL ${t.name}\n${e.stack}`);
      }
    });

    // Explicitly terminate, in case the debug port is open.
    if (isNode) {
      process.exit(0);
    }
  }

  before(t) {
    state.get(this).before = t;

    return this;
  }

  assert(expr) {
    if (!expr) {
      throw new Error('Assertion error');
    }
  }
}

if (isNode) {
  process.on('uncaughtException', e => console.error(e.stack));
}

function before(runner) {
  return state.get(runner).before || () => {};
}
